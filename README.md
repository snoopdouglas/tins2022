# TINS 2022 - Embervania

Hello! dthompson here, and this is my TINS 2022 entry. It's (the start of) a game I've been threatening to make for a while - mainly to some friends on Discord. More on that later. It's good that the rules permitted this because I already had some player code and sprite work in the repo before the competition started. ;)

It's a *tiny* Metroidvania - especially in its current deadline-adhering form. You should be able to complete it within 5 minutes - less if you skip the cutscene/dialogue.

It's about a rat named Ember who, along with her friends, been chucked deep into the sewers by some unknown entity.

![Screenshot](./.screenshot.png)

## Running the game

Binaries are in `dist/itch`, for reasons. Extract the package for your OS and run. It, ahem, should just work.

On Linux, you'll need `xz`.

### Controls

Use WASD or the arrow keys to move and X, L or Space to select or jump. This is explained in the (also tiny) tutorial though!

There are a few more things you can do:

* **F11** or **Alt+Enter**: toggle fullscreen
* **F12**: screenshot to `$HOME` (dev only)
* **Esc**: quit immediately (dev only)

## Rule adherence

### Genre

```
genre rule #102:
Theme: water / wet.
```

This is probably my weakest implementation. I mean there sure is water! ...but, like, it's not *everywhere*.

### Artistic

```
artistical rule #123:
You must integrate subliminal messages into your game.
```

You probably won't notice them right away, but there are some very dangerous and subliminal lyrics from Jerma's [Rats Birthday Mixtape](https://youtu.be/mNy9HIo3shs?t=35) in the background of the intro and teleport screens.

```
artistical rule #62:
Have teleporters in your game.
```

Not teleporters in the traditional sense - but certainly in the functional sense! Ember can move between levels (or sometimes between areas in the *same* level) by entering 'rat runs', shown as cracks in the wall.

### Technical

```
technical rule #108:
Implement and use any image post-processing filter.
```

Under the bonnet, the game uses full 32-bit colour - but for post-processing, I'm using a colour reduction and dithering shader. 6 shades per channel are allowed, giving a total of 216 allowed colours; block colour which doesn't dither therefore just needs to be chosen carefully (which is what I've done with the majority of the sprites). [Here's the code.](./src/core/Dither.gdshader)

### Bonus

```
bonus rule #28:
All your base are belong to us. You may modify or skip a rule if you cleverly use internet memes to explain your reasons.
```

One might argue that this entire game is a meme due to it being based on a bunch of nerds from [Ember's Discord server](https://discord.gg/ratting). Regardless, I'm not gonna bother invoking this rule. :p

## Acknowledgements

* Godot Engine - [license info here](https://godotengine.org/license)
* [Zapsplat](https://www.zapsplat.com/) - plenty of audio clips from here
* Dithering shader - used code from:
  * https://godotshaders.com/shader/color-reduction-and-dither/
  * https://godotshaders.com/shader/ps1-post-processing/

## Development

I'm using a modified version of the Feral Flowers build system, plus a few bits and pieces of GDScript that'll probably be useful (eg. persistent fullscreen toggling).

Develop on Linux. You'll need Docker (+ Compose) installed.

`cd` to the root of the repo, then:

```sh
bin/editor        # start godot editor
bin/run           # just run the game in debug mode
```

### Build

To run a build (for release or debug respectively):

```sh
bin/export/full
bin/export/debug
```

`dist/` will contain the built exports for Windows, Linux & macOS.

### Release procedure

* Bump the version in `src/core/Version.gd`
* Run `bin/export/full`
* Check the binary's alright with `bin/export-run`
* If all is well, tag and commit:
  ```sh
  VERSION=vX.Y.Z
  git add .
  git commit -m "$VERSION"
  git tag "$VERSION"
  git push
  git push --tags
  ```
* Run macOS codesigning - nb. this won't be documented here because it's a bespoke (and LAN-dependent) setup
* [TINS uploads are here](https://tins.amarillion.org/2022/upload/)

### License

This game's code and assets are MIT licensed except in the case of the audio where [Zapsplat's license](https://www.zapsplat.com/license-type/standard-license/) also applies.

```
Copyright © 2022 Douglas Thompson

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
```
