extends Node2D
class_name Door

onready var body := $StaticBody2D as StaticBody2D
onready var area := $Area2D as Area2D
onready var sprite := $Sprite as Sprite

var c: Counter = null
var open := false

func _ready():
	Util.aok(area.connect("body_entered", self, "_on_entered"))
	Util.aok(area.connect("body_exited", self, "_on_exited"))

func _on_entered(player: Player):
	if not (player and Game.card): return
	c = Counter.new(4)
	open = true
	body.collision_layer = 0

func _on_exited(player: Player):
	if not (player and Game.card): return
	c = Counter.new(4)
	open = false
	body.collision_layer = 1

func _physics_process(_delta):
	if c:
		var i := c.v - 1
		if c.decrement_stop():
			c = null
		sprite.scale.y = (
			i
			if open
			else (4 - i)
		)
