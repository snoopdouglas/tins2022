extends Node2D
class_name Collectibles

signal fx(instance)
signal cheezit
signal buff

const Collect = preload("res://fx/Collect.tscn")

func _ready():
	for nchild in get_children():
		var child := nchild as Collectible
		Util.aok(child.connect("body_entered", self, "_on_entered", [child]))

func _on_entered(player: Player, c: Collectible):
	if (not player) or Game.collectibles.has(c.uid):
		return

	Game.collectibles[c.uid] = true
	c._collected()
	if c is Cheezit:
		emit_signal("cheezit")
	else:
		emit_signal("buff", c.title)
	c.queue_free()
	var collect := Collect.instance() as Node2D
	collect.position = c.shape.global_position
#	if not (c is Cheezit):
#		collect.scale = Vector2.ONE * 2
	emit_signal("fx", collect)
