extends Area2D

func _ready():
	Util.aok(connect("body_entered", self, "_on_body_entered"))

func _on_body_entered(player: Player):
	if not player: return
	Game.tutorial_done = true

	set_deferred("collision_layer", 0)
	set_deferred("collision_mask", 0)

	var label := $Label as Label
	if player.position.x < 64:
		label.rect_position.x -= 96
	var tween := get_tree().create_tween()
	var _t := tween.tween_property(label, "modulate", Color.white, 0.3)
