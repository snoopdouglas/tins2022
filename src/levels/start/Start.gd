extends Level

onready var anim := $AnimationPlayer as AnimationPlayer

func _ready():
	var intro := $Intro as CanvasLayer
	if Game.tutorial_done:
		intro.queue_free()
		$FriendStand.queue_free()
		$FriendStand2.queue_free()
		$Move.queue_free()
		$Jump.queue_free()
		player.freeze = false
		bgm.play()
	else:
		intro.visible = true
		yield(get_tree().create_timer(1.0), "timeout")
		anim.play("Intro")
#		if DevPreview.preview_scene == self:
#			anim.seek(20)

func _input(event: InputEvent):
	if event.is_action_pressed("ui_select"):
		if (not Game.tutorial_done) and anim.is_playing() and (anim.current_animation_position < 19):
			anim.seek(19, true)
