extends Node2D
class_name NPCs

var current: NPC = null

func _ready():
	for nchild in get_children():
		var child := nchild as NPC
		Util.aok(child.connect("body_entered", self, "_on_entered", [child]))
		Util.aok(child.connect("body_exited", self, "_on_exited", [child]))

func _on_entered(player: Player, npc: NPC):
	if not player: return
	current = npc
	player._use_entered()

func _on_exited(player: Player, _npc: NPC):
	if not player: return
	current = null
	player._use_exited()

func _activate(player: Player):
	if current:
		current._activate(player)
