extends Node2D
class_name Level

export var level_name := ""

const BlackBg = preload("res://util/BlackBg.tscn")

onready var base_anim := $BaseAnimationPlayer as AnimationPlayer
onready var player := $Player as Player
onready var fx := $FX as Node2D
onready var camera := $LevelCamera as LevelCamera
onready var ratruns := $Ratruns as Ratruns
onready var npcs := $NPCs as NPCs
onready var collectibles := $Collectibles as Collectibles
onready var announce := $Static/Announce as Label
onready var fade := $Static/Fade as Polygon2D
#onready var water := $Water as Area2D
onready var cheezits := $Static/Cheezits as Sprite
onready var cheezits_count := $Static/Cheezits/Count as Label
onready var collect_announce_label := $Static/CollectAnnounce/Label as Label
onready var bgm := $Bgm as AudioStreamPlayer

var cheezit_timer := 0

func _ready():
	Util.aok(player.connect("fx", self, "_on_fx"))
	Util.aok(player.connect("landed", camera, "_manual_y_scroll"))
	Util.aok(player.connect("using", self, "_on_player_using"))
	camera.target = player.camera_target

	Util.aok(collectibles.connect("fx", self, "_on_fx"))
	Util.aok(collectibles.connect("cheezit", self, "_on_cheezit"))
	Util.aok(collectibles.connect("buff", self, "_on_buff"))

	cheezits.visible = true
	if Game.ratrun_path_full() == filename:
		for nr in ratruns.get_children():
			var r := nr as Ratrun
			if (r.pair == Game.ratrun_pair) and (Game.ratrun_same_level != r.name):
				player.position = r.position
				camera._snap()
				fade.modulate = Color.white
				var tween := get_tree().create_tween()
				var _t := tween.tween_property(fade, "modulate", Color.transparent, 0.15)
				announce.text = (
					r.level_name_override
					if r.level_name_override
					else level_name
				)
				var _u := tween.tween_interval(1.0)
				var _v := tween.tween_callback(announce, "set_visible", [false])
				if not Bgm.playing:
					Bgm.play()
				break

func _physics_process(_delta):
	if cheezit_timer:
		cheezit_timer -= 1
		cheezits.position.y = max(120, cheezits.position.y - 1)
	else:
		cheezits.position.y = min(136, cheezits.position.y + 1)

func _on_fx(node: Node2D):
	fx.add_child(node)

func _on_player_using():
	if ratruns.current:
		player._pre_ratrun()

		var tween := get_tree().create_tween()
		var _t := tween.tween_property(fade, "modulate", Color.white, 0.15).set_delay(0.3)
		var _u := tween.tween_callback(ratruns, "_activate")
	elif npcs.current:
		npcs._activate(player)

func _on_cheezit():
	cheezit_timer = 120
	cheezits_count.text = String(Game.cheezits)

func _on_buff(title: String):
	collect_announce_label.text = title
	base_anim.play("CollectAnnounce")
