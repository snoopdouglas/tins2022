extends Node2D
class_name Ratruns

var current: Ratrun = null

func _ready():
	for nchild in get_children():
		var child := nchild as Ratrun
		Util.aok(child.connect("body_entered", self, "_on_entered", [child]))
		Util.aok(child.connect("body_exited", self, "_on_exited", [child]))

func _on_entered(player: Player, ratrun: Ratrun):
	if not player: return
	current = ratrun
	player._use_entered()

func _on_exited(player: Player, _ratrun: Ratrun):
	if not player: return
	current = null
	player._use_exited()

func _activate():
	if current:
		Game.ratrun_same_level = (
			current.name
			if (current.destination == Game.ratrun_path)
			else ""
		)
		Game.ratrun_path = current.destination
		Game.ratrun_pair = current.pair
		SceneChange.go("res://warp/Warp.tscn")
