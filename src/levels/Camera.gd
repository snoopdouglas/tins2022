extends Camera2D
class_name LevelCamera

const SMOOTHING_FRAMES = 22
const THRESHOLD := Vector2(10.0, 45.0)
const THRESHOLD_DOWN := 15.0
const CENTER_OFFSET = Vector2(120, 64)

var target: Node2D = null setget set_target

var center := Vector2()
var static_prev := Vector2()
var target_prev := Vector2()
var smoothing_x := 0
var smoothing_y := 0
var following := Vector2.ZERO

func set_target(node: Node2D):
	if target:
		# if we've just targeted something else, smoothly move to it
		static_prev = center
		smoothing_x = SMOOTHING_FRAMES
		smoothing_y = SMOOTHING_FRAMES
	else:
		# otherwise, move immediately - this is probably init
		center = node.global_position

	target = node
	target_prev = node.global_position

func _snap():
	center = target.global_position

func smoothing_ease(i: int) -> float:
	return ease(1.0 - (i / float(SMOOTHING_FRAMES)), 0.5)

func _physics_process(_delta):
	if not target: return

	var pos := target.global_position
	var d := pos - target_prev
	var d_manhattan := Vector2(
		1 if (d.x > 0) else -1,
		1 if (d.y > 0) else -1
	)

	if smoothing_x:
		var l := smoothing_ease(smoothing_x)
		center.x = lerp(static_prev.x, pos.x, l)
		smoothing_x -= 1
		following.x = d_manhattan.x
	else:
		if d.x and (following.x == d_manhattan.x):
			center.x = pos.x
		else:
			following.x = 0
			if abs(center.x - pos.x) >= THRESHOLD.x:
				static_prev.x = center.x
				smoothing_x = SMOOTHING_FRAMES

	if smoothing_y:
		var l := smoothing_ease(smoothing_y)
		center.y = lerp(static_prev.y, pos.y, l)
		smoothing_y -= 1
		following.y = d_manhattan.y
	else:
		if d.y and (following.y == d_manhattan.y):
			center.y = pos.y
		else:
			following.y = 0
			# NOTE: special case, if going downwards we use a lower threshold
			var diff := center.y - pos.y
			if (abs(diff) >= THRESHOLD.y) or (diff <= -THRESHOLD_DOWN):
				static_prev.y = center.y
				smoothing_y = SMOOTHING_FRAMES

	target_prev = pos
	position = (center - CENTER_OFFSET).round()

# called when player lands on a surface
func _manual_y_scroll():
	if not (smoothing_y or following.y):
		static_prev.y = center.y
		smoothing_y = SMOOTHING_FRAMES
