extends Area2D

func _ready():
	Util.aok(connect("body_entered", self, "_on_body_entered"))
	Util.aok(connect("body_exited", self, "_on_body_exited"))

func _on_body_entered(player: Player):
	if not player: return
	player._water_entered()

func _on_body_exited(player: Player):
	if not player: return
	player._water_exited()
