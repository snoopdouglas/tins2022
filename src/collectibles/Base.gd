tool
extends Area2D
class_name Collectible

const FLOAT_FRAMES = 80

export var title := ""
export var uid := 0

onready var sprite := $Sprite as Sprite
onready var shape := $CollisionShape2D as CollisionShape2D

var t := 0

func _ready():
	if Engine.editor_hint:
		var bytes := Crypto.new().generate_random_bytes(8)
		uid = 0
		for i in 8:
			uid |= (bytes[i] << (i * 8))
	else:
		if Game.collectibles.has(uid):
			queue_free()
		else:
			#warning-ignore:narrowing_conversion
			t = 10 * ((position.x) / 8)
			t %= FLOAT_FRAMES

func _physics_process(_delta):
	if not Engine.editor_hint:
		t += 1
		sprite.position.y = sin((t / float(FLOAT_FRAMES)) * TAU) * 2

func _collected():
	pass # virtual
