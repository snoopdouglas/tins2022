extends Polygon2D

var i := 8

func _physics_process(_delta):
	i -= 1
	#warning-ignore:integer_division
	scale = Vector2.ONE * (1 + ((i / 2) * 2))
	if i == 0:
		queue_free()
