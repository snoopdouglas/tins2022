extends Node2D

func _ready():
	yield(get_tree().create_timer(0.4), "timeout")
	($AnimationPlayer as AnimationPlayer).play("Intro")

func anim_advance():
	SceneChange.go("res://menu/Menu.tscn")
