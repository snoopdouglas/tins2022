extends Node

const SBoot = preload("res://core/Boot.tscn")

var preview_scene: Node

func _enter_tree():
	if not OS.is_debug_build():
		queue_free()
	else:
		var tree := get_tree()
		yield(tree, "idle_frame")
		var child := tree.root.get_child(tree.root.get_child_count() - 1)
		if not (child is Boot):
			tree.root.remove_child(child)
			preview_scene = child
			tree.root.add_child(SBoot.instance())
		yield(tree, "idle_frame")
