extends Node

signal go

func go(path: String):
	print("[scene] " + path)
	var scene := load(path) as PackedScene
	emit_signal("go", scene)
