extends Node

# global game state

var tutorial_done: bool
var ratrun_path: String
var ratrun_pair: String
var ratrun_same_level: String
var collectibles: Dictionary
var cheezits: int
var card: bool

func reset():
	tutorial_done = false
	ratrun_path = ""
	ratrun_pair = ""
	ratrun_same_level = ""
	collectibles = {}
	cheezits = 0
	card = false

func ratrun_path_full() -> String:
	return "res://levels/" + ratrun_path + ".tscn"
