extends Node

# loads resources on a thread. largely stolen from the threaded loader in
# godot's docs, but with a couple of bugfixes and strong typing.
# add this as an autoload singleton to get started.

# usage:
# - use queue_resource(path) to begin loading
# - check is_ready(path) periodically
# - get_progress(path) might be useful for big resources
# - once ready, use get_resource(path)
# - failed loads will return -1 (see below)

const FAILED = -1

var thread: Thread
var mutex: Mutex
var sem: Semaphore

var queue := []
var pending := {}
var quit := false

func _enter_tree():
	mutex = Mutex.new()
	sem = Semaphore.new()
	thread = Thread.new()

	var k := OK == thread.start(self, "thread_func")
	assert(k)

func _exit_tree():
	_lock("_exit_tree")
	quit = true
	_post("_exit_tree")
	_unlock("_exit_tree")
	thread.wait_to_finish()


func _lock(_caller):
	mutex.lock()

func _unlock(_caller):
	mutex.unlock()

func _post(_caller):
	#warning-ignore:return_value_discarded
	sem.post()

func _wait(_caller):
	#warning-ignore:return_value_discarded
	sem.wait()

func log_msg(msg: String, stderr: bool = false):
	msg = "[rq] " + msg
	if stderr:
		printerr(msg)
	else:
		if OS.is_debug_build():
			print_debug(msg)

func queue_resource(path: String, p_in_front := false):
	_lock("queue_resource")
	if path in pending:
		_unlock("queue_resource")
		return
	elif ResourceLoader.has_cached(path):
		log_msg("cached: " + path)
		pending[path] = ResourceLoader.load(path)
		_unlock("queue_resource")
		return
	else:
		log_msg("queuing: " + path)
		var loader := ResourceLoader.load_interactive(path)
		if loader:
			loader.set_meta("path", path)
			if p_in_front:
				queue.insert(0, loader)
			else:
				queue.push_back(loader)
			pending[path] = loader
		else:
			log_msg("couldn't interactively load: " + path, true)
			pending[path] = FAILED
		_post("queue_resource")
		_unlock("queue_resource")
		return


func cancel_resource(path: String):
	_lock("cancel_resource")
	if path in pending:
		var loader := pending[path] as ResourceInteractiveLoader
		if loader:
			queue.erase(loader)
		#warning-ignore:return_value_discarded
		pending.erase(path)
	_unlock("cancel_resource")


func get_progress(path: String):
	_lock("get_progress")
	var ret := -1.0
	if path in pending:
		var loader := pending[path] as ResourceInteractiveLoader
		if loader:
			ret = float(loader.get_stage()) / float(loader.get_stage_count())
		else:
			ret = 1.0
	_unlock("get_progress")
	return ret


func is_ready(path: String):
	var ret := false
	_lock("is_ready")
	if path in pending:
		ret = not (pending[path] is ResourceInteractiveLoader)
	_unlock("is_ready")
	return ret


func _wait_for_resource(loader: ResourceInteractiveLoader, path: String) -> Resource:
	_unlock("wait_for_resource")
	log_msg("warning: blocking to load " + path, true)
	while true:
		VisualServer.sync()
		OS.delay_usec(16000) # Wait approximately 1 frame.
		_lock("wait_for_resource")
		if (queue.size() == 0) or (queue[0] != loader):
			return pending[path]
		_unlock("wait_for_resource")

	assert(false)
	return null # required syntax


func get_resource(path: String) -> Resource:
	_lock("get_resource")
	if path in pending:
		if pending[path] is ResourceInteractiveLoader:
			var loader := pending[path] as ResourceInteractiveLoader
			if loader != queue[0]:
				var pos := queue.find(loader)
				queue.remove(pos)
				queue.insert(0, loader)

			var res := _wait_for_resource(loader, path)
			#warning-ignore:return_value_discarded
			pending.erase(path)
			_unlock("get_resource")
			return res
		else:
			var res: Resource = null
			if pending[path] is Resource:
				res = pending[path]
			#warning-ignore:return_value_discarded
			pending.erase(path)
			_unlock("get_resource")
			return res
	else:
		_unlock("get_resource")
		log_msg("warning: blocking to load " + path, true)
		return ResourceLoader.load(path)


func thread_func(_u):
	log_msg("up")

	while true:
		_wait("thread_func")
		_lock("thread_func")

		if quit:
			_unlock("thread_func")
			break

		while queue.size() > 0:
			var loader := queue[0] as ResourceInteractiveLoader
			_unlock("thread_func_poll_start")
			var ret := loader.poll()
			_lock("thread_func_poll_finish")

			if ret != OK:
				var path := loader.get_meta("path") as String
				if ret == ERR_FILE_EOF:
					log_msg("loaded: " + path)
				else:
					log_msg("load failed: %s (%d)" % [path, ret], true)
				if path in pending: # Else, it was already retrieved.
					# nb. this will be null if load failed
					pending[path] = loader.get_resource()
				# Something might have been put at the front of the queue while
				# we polled, so use erase instead of remove.
				queue.erase(loader)
		_unlock("thread_func")
