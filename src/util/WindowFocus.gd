extends Node

# for if ya don't like notifications.
# setup: add as an autoload singleton.

signal toggled(on)

func _notification(what: int):
	match what:
		NOTIFICATION_WM_FOCUS_OUT:
			emit_signal("toggled", false)
		NOTIFICATION_WM_FOCUS_IN:
			emit_signal("toggled", true)
