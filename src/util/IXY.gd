extends Reference
class_name IXY

# Vector2 but in Manhattan

var x: int
var y: int

func _init(nx := 0, ny := 0):
	x = nx
	y = ny

static func from_vec2(v: Vector2) -> IXY:
	return IXY.new(round(v.x) as int, round(v.y) as int)
static func zero() -> IXY:
	# actually the same as new() - this is just for semantics
	return IXY.new(0, 0)

func add(b: IXY) -> IXY:
	return IXY.new(x + b.x, y + b.y)
func sub(b: IXY) -> IXY:
	return IXY.new(x - b.x, y - b.y)

func normalise() -> IXY:
	var xn := 0
	var yn := 0
	if x != 0:
		xn = 1 if (x > 0) else -1
	if y != 0:
		yn = 1 if (y > 0) else -1
	return IXY.new(xn, yn)

func copy(b: IXY):
	x = b.x
	y = b.y
func clone() -> IXY:
	return IXY.new(x, y)
