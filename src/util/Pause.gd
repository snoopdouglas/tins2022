extends Node

# general pause utility. this does two things:
# - pauses when:
#   - the "pause" input action is pressed
#   - _toggle(...) is called directly
#   - focus has been lost for a while
#     (nb. this is to prevent pausing on screenshots / volume changes on X11)
# - buffers pausing to occur as soon as the 'pausable' flag is true

# setup:
# - add this as an autoload singleton
# - add a 'pause' input action
# - connect a pause menu (or something) to the 'toggled' signal
# - adjust the below constants as necessary
#   (nb. PRIORITY should be high enough that 'just_unpaused' is effective)

signal toggled(on)

const BUFFER_TIME = 0.5
const FOCUS_DEBOUNCE_TIME = 0.5
const PRIORITY = 1000

var pausable := false setget set_pausable
var just_unpaused := false
var buffer := 0.0
var blur_timer := 0.0
var blur_buffer := false

func _enter_tree():
	pause_mode = PAUSE_MODE_PROCESS
	process_priority = PRIORITY

func set_pausable(p: bool):
	if pausable == p:
		return
	pausable = p

	if pausable and (buffer or blur_buffer):
		_toggle(true)

func _toggle(on: bool):
	var tree := get_tree()
	if on == tree.paused:
		return

	if on:
		if pausable:
			do_toggle(true)
		else:
			buffer = BUFFER_TIME
	else:
		do_toggle(false)

func do_toggle(on: bool):
	buffer = 0
	blur_timer = 0
	blur_buffer = false
	get_tree().paused = on
	just_unpaused = not on
	emit_signal("toggled", on)

func _input(event: InputEvent):
	if event.is_action_pressed("pause"):
		_toggle(not get_tree().paused)

func _notification(what: int):
	match what:
		NOTIFICATION_WM_FOCUS_OUT:
			blur_timer = FOCUS_DEBOUNCE_TIME
		NOTIFICATION_WM_FOCUS_IN:
			blur_timer = 0
			blur_buffer = false

func _physics_process(delta: float):
	buffer = max(buffer - delta, 0.0)

	if blur_timer:
		blur_timer = max(blur_timer - delta, 0.0)
		if blur_timer == 0.0:
			if pausable:
				_toggle(true)
			else:
				blur_buffer = true

	just_unpaused = false
