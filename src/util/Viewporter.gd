extends Node2D
class_name Viewporter

export var size := Vector2(320, 180)
export var usage: int = Viewport.USAGE_2D_NO_SAMPLING setget set_usage

onready var viewport := Viewport.new()
onready var sprite := Sprite.new()

func _ready():
	for nchild in get_children():
		var child := nchild as Node
		.remove_child(child)
		viewport.add_child(child)

	viewport.size = Vector2(320, 180)
	viewport.usage = usage
	viewport.render_target_v_flip = true
	sprite.texture = viewport.get_texture()
	sprite.centered = false

	.add_child(viewport)
	.add_child(sprite)

func add_child(node: Node, legible_unique_name := false):
	viewport.add_child(node, legible_unique_name)
func remove_child(node: Node):
	viewport.remove_child(node)

func _input(event: InputEvent):
	viewport.input(event)

func set_size(s: Vector2):
	size = s
	if viewport:
		viewport.size = size
func set_usage(u: int):
	usage = u
	if viewport:
		viewport.usage = usage
