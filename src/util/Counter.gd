extends Reference
class_name Counter

var v: int
var initial: int

func _init(initial_value: int, oneshot := 0):
	setup(initial_value, oneshot)

func setup(initial_value: int, oneshot := 0):
	v = oneshot if oneshot else initial_value
	initial = initial_value

func reset():
	v = initial

func decrement_stop() -> bool:
	assert(v >= 0)
	if not v:
		return false
	v -= 1
	return not v

# no return, means we don't need to dodge discarded return type warnings where
# point of loop isn't observed
func decrement_stopn():
	var _x := decrement_stop()

func decrement_loop() -> bool:
	assert(v > 0)
	v -= 1
	if v == 0:
		v = initial
		return true
	return false

func decrement_loopn():
	var _x := decrement_loop()

func progress_inv() -> float:
	return float(v) / float(initial)
func progress() -> float:
	return 1.0 - progress_inv()

func lerp(from: float, to: float) -> float:
	var pinv := progress_inv()
	return (from * pinv) + (to * (1.0 - pinv)) # optimised

func flicker(mod: int, n: int) -> bool:
	return (v % mod) < n

func protected(_x):
	assert(false)
