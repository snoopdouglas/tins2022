extends Object
class_name Util

# very generic util lib

# 'assert ok': assert that an some error didn't occur - useful for preventing
# warnings on connect(...) etc.
static func aok(err: int):
	assert(err == OK)

static func grey(v: float, a := 1.0) -> Color:
	return Color(v, v, v, a)
