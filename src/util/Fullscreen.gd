extends Node

# fullscreen toggle, persisted to disk. requires some setup:
# - add this as an autoload singleton
# - start the game in windowed mode
# - add a 'fullscreen' input action

const WINDOW_SIZE_DEFAULT = Vector2(1200, 675)

# nb. these are variables because they might need to be set at runtime
var fullscreen_default := false
var window_min_size := OS.window_size
var mouse_visible := false

func _enter_tree():
	# remove these lines if you're sure:
	if OS.has_feature("HTML5"):
		queue_free()
		return

	OS.min_window_size = window_min_size

	if not OS.window_fullscreen:
		OS.window_size = WINDOW_SIZE_DEFAULT
		OS.call_deferred("center_window")

	var f := File.new()
	var k := OK == f.open("user://fullscreen", File.READ)
	OS.window_fullscreen = (
		bool(f.get_8())
		if k
		# fail silently
		else fullscreen_default
	)

	# the 'streamer fix' - make windowed if primary monitor is portrait
	if OS.window_fullscreen:
		if OS.window_size.y > OS.window_size.x:
			OS.window_fullscreen = false

	setup_window()

func _exit_tree():
	var f := File.new()
	var k := OK == f.open("user://fullscreen", File.WRITE)
	if k:
		# fail silently
		f.store_8(int(OS.window_fullscreen))

func setup_window():
	# mouse visibility
	Input.set_mouse_mode(
		Input.MOUSE_MODE_VISIBLE
		if mouse_visible
		else (
			Input.MOUSE_MODE_HIDDEN
			if OS.window_fullscreen
			else Input.MOUSE_MODE_VISIBLE
		)
	)

	if not OS.window_fullscreen:
		OS.window_size = WINDOW_SIZE_DEFAULT
		OS.call_deferred("center_window")

func _input(event: InputEvent):
	if event.is_action_pressed("fullscreen"):
		OS.window_fullscreen = not OS.window_fullscreen
		setup_window()
