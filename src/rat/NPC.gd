extends Area2D
class_name NPC

export var character_name := ""
export var lines := PoolStringArray()

enum State {
	INACTIVE,
	TYPING,
	WAITING,
}

onready var sprite := $Sprite as Sprite
onready var layer := $CanvasLayer as CanvasLayer
onready var label_name := $CanvasLayer/Name as Label
onready var label_dialogue := $CanvasLayer/Dialogue as Label
onready var next_icon := $CanvasLayer/Next as Polygon2D
onready var sfx_talk := $Talk as AudioStreamPlayer
onready var sfx_talk_fast := $TalkFast as AudioStreamPlayer

var state: int = State.INACTIVE
var player_counter: Counter = null
var state_counter: Counter = null
var line_i := 0
var player: Player
var player_old_pos: Vector2
var player_want_pos: Vector2
var fast := false

func _ready():
	label_name.text = character_name
	player_want_pos = position + Vector2(-16 if sprite.flip_h else 16, 0)

func _physics_process(_delta):
	if player_counter and player_counter.v:
		player_counter.decrement_stopn()
		var p := player_counter.progress()
		player.position = Vector2(
			lerp(player_old_pos.x, player_want_pos.x, p),
			player_want_pos.y - (sin(p * PI) * 5)
		)
	else:
		match state:
			State.TYPING:
				if state_counter.decrement_loop() or fast:
					label_dialogue.visible_characters += 1
					if (label_dialogue.visible_characters == 2) and Input.is_action_pressed("ui_select"):
						# make it faster if player is mashin
						fast = true
						sfx_talk.stop()
						sfx_talk_fast.play()
					if label_dialogue.visible_characters == label_dialogue.text.length():
						state = State.WAITING
						state_counter = Counter.new(15)
						next_icon.visible = true
						sfx_talk.stop()
						sfx_talk_fast.stop()
			State.WAITING:
				if state_counter.decrement_loop():
					next_icon.visible = not next_icon.visible

func _input(event: InputEvent):
	match state:
		State.TYPING:
			if event.is_action_pressed("ui_select"):
				fast = true
				sfx_talk.stop()
				sfx_talk_fast.play()
			elif event.is_action_released("ui_select"):
				fast = false
				sfx_talk.play()
				sfx_talk_fast.stop()
		State.WAITING:
			if event.is_action_pressed("ui_select"):
				line_i += 1
				next_icon.visible = false
				if line_i == lines.size():
					_deactivate()
				else:
					reset_line()

func reset_line():
	state = State.TYPING
	label_dialogue.text = lines[line_i]
	label_dialogue.visible_characters = 0
	fast = false
	state_counter = Counter.new(5)
	sfx_talk.play()

func _activate(nplayer: Player):
	_pre_activate()
	player = nplayer

	player.freeze = true
	player.sprite.animation = "idle"
	player.sprite.flip_h = not sprite.flip_h
	player.d = Vector2.ZERO
	player.sfx_walk.stop()

	player_old_pos = player.position
	if player_old_pos.round() != player_want_pos.round():
		player_counter = Counter.new(12)
		yield(get_tree().create_timer(0.5), "timeout")

	layer.visible = true
	$CanvasLayer/TileMap.visible = true # engine bug hack
	line_i = 0
	reset_line()

func _deactivate():
	state = State.INACTIVE
	player.freeze = false
	layer.visible = false
	sfx_talk.stop()
	sfx_talk_fast.stop()

func _pre_activate():
	pass # virtual
