extends Node2D

func _ready():
	for rats in [$Rats, $Rats2]:
		var a := ["jump", "shake", "sleep", "walk"][randi() % 4] as String
		for nsprite in (rats as Node).get_children():
			var sprite := nsprite as AnimatedSprite
			sprite.animation = a
			sprite.frames.set_animation_loop(a, true)
			sprite.play()

func anim_advance():
	SceneChange.go(Game.ratrun_path_full())
