FROM debian:bullseye-slim AS downloader

RUN apt-get update && apt-get install -y curl unzip

ARG GODOT_VERSION
ARG GODOT_SUFFIX
ARG GODOT_SUBDIR

RUN curl -L --fail "https://downloads.tuxfamily.org/godotengine/${GODOT_VERSION}/${GODOT_SUBDIR}Godot_v${GODOT_VERSION}-${GODOT_SUFFIX}_linux_headless.64.zip" > godot.zip && \
  unzip godot.zip && \
  mv "Godot_v${GODOT_VERSION}-${GODOT_SUFFIX}_linux_headless.64" /usr/bin/godot && \
  rm godot.zip

RUN curl -L --fail "https://downloads.tuxfamily.org/godotengine/${GODOT_VERSION}/${GODOT_SUBDIR}Godot_v${GODOT_VERSION}-${GODOT_SUFFIX}_export_templates.tpz" > export.zip && \
  cd /root && \
  mkdir -p .local/share/godot/templates && \
  cd .local/share/godot/templates && \
  unzip /export.zip && \
  mv templates "${GODOT_VERSION}.${GODOT_SUFFIX}" && \
  rm /export.zip

RUN curl -L --fail "https://github.com/electron/rcedit/releases/download/v1.1.1/rcedit-x64.exe" > /rcedit-x64.exe

RUN mkdir /opt/butler && \
  cd /opt/butler && \
  curl -L --fail "https://broth.itch.ovh/butler/linux-amd64/15.21.0/archive/default" > butler.zip && \
  unzip butler.zip && \
  chmod +x butler && \
  rm butler.zip

# ---

FROM debian:bullseye-slim

RUN dpkg --add-architecture i386 && \
  apt-get update && \
  ( export INSTALL_CMD='apt-get install -y curl zip unzip imagemagick wine64 wine32 p7zip-full xz-utils' && \
  ( $INSTALL_CMD || $INSTALL_CMD || $INSTALL_CMD ) )

ENV PATH="/opt/butler:${PATH}"

COPY --from=downloader /usr/bin/godot /usr/bin/godot
COPY --from=downloader /root/.local/share/godot/templates /root/.local/share/godot/templates
COPY --from=downloader /rcedit-x64.exe /rcedit-x64.exe
COPY --from=downloader /opt/butler /opt/butler

# note: `godot --version` exits 255 for some reason
RUN ( /usr/bin/godot --version || [ "$?" -eq 255 ] ) && \
  wine /rcedit-x64.exe && \
  butler --help

RUN mkdir -p /root/.config/godot
COPY editor_settings-3.tres /root/.config/godot/editor_settings-3.tres
