#!/bin/bash
set -eo pipefail

cd /srv/src

VERSION="$(sed -E -n 's/^var game := "(.+)"$/\1/p' core/Version.gd)"
if [ -z "$VERSION" ]; then
  echo "error: couldn't get version from src/core/Version.gd"
  exit 1
fi

function sed-presets {
  sed -E -i $@ export_presets.cfg
}

sed-presets "s/^(application\/(file_version|product_version|short_version|version|info))=\".*\"$/\1=\"$VERSION\"/g"
sed-presets "s/^(version\/name)=\".+\"$/\1=\"$VERSION\"/g"

(
  cd core
  convert icon.png -define icon:auto-resize=256,128,64,48,32,16 icon.ico
)

cd /srv
rm -rf dist
mkdir dist
cd dist
mkdir itch unsigned
cd unsigned
mkdir itch
