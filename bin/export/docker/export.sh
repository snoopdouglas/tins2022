#!/bin/bash
set -eo pipefail

[ "$T22_CUT" != null ]

cp -r /srv/src /build
cd /build

EXPORT_FLAGS='--export'
DIST_BASE_NAME="TINS2022-$T22_VERSION"
DIST_SUBDIR='itch'

if [ "$T22_CUT" == debug ]; then
  EXPORT_FLAGS='--export-debug'
  DIST_BASE_NAME="$DIST_BASE_NAME-DEBUG"
fi

if [ "$T22_OS" == macos ]; then
  DIST_SUBDIR="unsigned/$DIST_SUBDIR"
fi

DIST_DIR="/srv/dist/$DIST_SUBDIR"
EXPORT_FLAGS="$EXPORT_FLAGS --quiet"

mkdir /out

if [ "$T22_OS" == windows ]; then
  EXPORT_BASE="$DIST_BASE_NAME-x86_64"

  godot $EXPORT_FLAGS windows "/out/$EXPORT_BASE.exe"

  cd /out
  7z a -tzip "$DIST_DIR/$DIST_BASE_NAME-windows-x86_64.zip" * > /dev/null

elif [ "$T22_OS" == linux ]; then
  EXPORT_BASE="$DIST_BASE_NAME.x86_64"

  godot $EXPORT_FLAGS linux "/out/$EXPORT_BASE"

  XZ_OPT='-T0' tar -cJf "$DIST_DIR/$DIST_BASE_NAME-linux-x86_64.tar.xz" -C /out $(ls /out)

elif [ "$T22_OS" == macos ]; then
  # godot zips this for us
  godot $EXPORT_FLAGS macos "$DIST_DIR/$DIST_BASE_NAME-mac-universal.zip"
fi
